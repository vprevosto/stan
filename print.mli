(* This file is distributed under the MIT license.
   See file LICENSE for more details.
   © 2018-2022 CEA
*)

(** pretty-printer for a program. *)
open Ast

val pp_location: Format.formatter -> location -> unit

val pp_arith_expr: Format.formatter -> arith_expr -> unit

val pp_boolean_expr: Format.formatter -> boolean_expr -> unit

val pp_expr: Format.formatter -> expr -> unit

val pp_instruction: Format.formatter -> instruction -> unit
