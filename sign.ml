(* This file is distributed under the MIT license.
   See file LICENSE for more details.
   © 2018-2022 CEA
*)

open Ast

type sign = Bot | Z | NZ | SNEG | NEG | SPOS | POS | Err | Top

let join_sign s1 s2 = failwith "writeme" [@@ ocaml.warning "-27"]

let sign_of_int z =
  if Z.equal Z.zero z then Z
  else if Z.lt Z.zero z then SPOS
  else SNEG

module Env = Map.Make(String)

type env = sign Env.t

let empty_env = Env.empty

let string_of_sign =
  function
    | Bot -> "Bot"
    | NZ -> "NZ"
    | Z -> "Z"
    | SNEG -> "SNEG"
    | NEG -> "NEG"
    | SPOS -> "SPOS"
    | POS -> "POS"
    | Err -> "Err"
    | Top -> "Top"

let pretty_var out name v =
  Printf.fprintf out "%s = %s\n" name (string_of_sign v)

let pretty_env out env =
  Env.iter (pretty_var out) env

let join_value _ v1 v2 = Some (join_sign v1 v2)

let join_env env1 env2 = Env.union join_value env1 env2

let included_env env1 env2 =
  let included_val v1 v2 =
    let v' = join_sign v1 v2 in v2 = v'
  in
  let included_var x v1 =
    match Env.find_opt x env2 with
      | Some v2 -> included_val v1 v2
      | None -> false
  in
  Env.for_all included_var env1

let equal_env env1 env2 = included_env env1 env2 && included_env env2 env1

let find_val env x =
  match Env.find_opt x env with
    | None -> Bot
    | Some s -> s

let add s1 s2 = failwith "writeme" [@@ ocaml.warning "-27"]

let mul s1 s2 = failwith "writeme" [@@ ocaml.warning "-27"]

let div s1 s2 = failwith "writeme" [@@ ocaml.warning "-27"]

let rec eval_arith env = function
  | Cons n -> sign_of_int n
  | Loc (Var v) -> find_val env v
  | Loc (Mem _) -> Top
  | Add(e1,e2) -> eval_binop env add e1 e2
  | Mul(e1,e2) -> eval_binop env mul e1 e2
  | Div(e1,e2) -> eval_binop env div e1 e2
  | Opp e -> failwith "writeme" [@@ ocaml.warning "-27"]

and eval_binop env op e1 e2 =
  op (eval_arith env e1) (eval_arith env e2)

let sign_equal z1 z2 =
  match (z1,z2) with
    | Bot, _ | _, Bot
      | Err, _ | _, Err -> Some false
    | Z, Z -> Some true
    | _ -> None

let sign_lt z1 z2 =
  match (z1, z2) with
    | Bot, _ | _, Bot -> Some false
    | Err, _ | _, Err -> Some false
    | Top, _ | _, Top -> None
    | NZ, _ | _, NZ -> None
    | SNEG, SNEG | SNEG, NEG -> None
    | SNEG, Z | SNEG, POS | SNEG, SPOS -> Some true
    | NEG, SNEG | NEG, NEG | NEG, Z | NEG, POS -> None
    | NEG, SPOS -> Some true
    | Z, SNEG -> Some false
    | Z, NEG | Z, POS | Z, Z -> None
    | Z, SPOS -> Some true
    | POS, SNEG -> Some false
    | POS, Z | POS, NEG | POS, POS | POS, SPOS -> None
    | SPOS, SNEG | SPOS, Z | SPOS, NEG -> Some false
    | SPOS, POS | SPOS, SPOS -> None

let sign_le z1 z2 =
  match sign_lt z1 z2 with
    | None | Some false -> sign_equal z1 z2
    | Some true -> Some true

let cmp op z1 z2 =
  match op with
    | Eq -> sign_equal z1 z2
    | Lt -> sign_lt z1 z2
    | Le -> sign_le z1 z2
    | Ge -> sign_le z2 z1
    | Gt -> sign_lt z2 z1
    | Ne -> (match sign_equal z1 z2 with None -> None | Some b -> Some (not b))

let rec eval_bool env = function
  | BCons b -> Some b
  | Comp(op, e1, e2) ->
      cmp op (eval_arith env e1) (eval_arith env e2)
  | And (e1,e2) ->
      (match eval_bool env e1, eval_bool env e2 with
         | Some b1, Some b2 -> Some (b1 && b2)
         | None, _ | _, None -> None)
    | Or(e1, e2) ->
      (match eval_bool env e1, eval_bool env e2 with
         | Some b1, Some b2 -> Some (b1 || b2)
         | None, _ | _, None -> None)
    | Not e ->
        (match eval_bool env e with
           | Some b -> Some (not b)
           | None -> None)

let eval_expr env = function
  | Arith e -> eval_arith env e
  | Address _ -> Top

let reduce_cmp op x v env =
  let orig =
    match Env.find_opt x env with
      | Some v -> v
      | None -> Bot
  in
  let new_v = failwith "writeme" in
  Env.add x new_v env
[@@ ocaml.warning "-27-26"]

let negate = function
  | Eq -> Ne
  | Ne -> Eq
  | Lt -> Ge
  | Le -> Gt
  | Ge -> Lt
  | Gt -> Le

let reduce_test b env =
  let rec aux is_pos b env =
    match b with
      | BCons _ -> env
      | Comp(op, Loc (Var v), e) ->
          let op = if is_pos then op else negate op in
          let res = eval_arith env e in
          reduce_cmp op v res env
      | Comp _ -> env
      | And (e1, e2) ->
          if is_pos then aux is_pos e2 (aux is_pos e1 env) else env
      | Or (e1,e2) -> if is_pos then env else aux is_pos e2 (aux is_pos e1 env)
      | Not e -> aux (not is_pos) e env
  in aux true b env

module Sign =
  struct
    type t = env
    type edge = Stan.Cfg.edge
    let join = join_env
    let equal = equal_env
    let analyze e env =
      if Env.is_empty env then empty_env
      else begin
          match (Stan.Cfg.E.label e) with
            | Stan.Tskip -> env
            | Stan.Ttest b ->
                (match eval_bool env b with
                   | None -> reduce_test b env
                   | Some true -> env
                   | Some false -> empty_env)
            | Stan.Tset (Var x, e) ->
                (match eval_expr env e with
                   | Bot | Err -> empty_env
                   | v -> Env.add x v env)
            | Stan.Tset (Mem _,e) ->
                (match eval_expr env e with
                   | Bot | Err -> empty_env
                   | v ->
                       (* we can update any variable with v *)
                       Env.fold
                         (fun x old_v env ->
                           Env.add x (join_sign old_v v) env)
                         env Env.empty)
        end

    let widening = join
    let bot = empty_env
    let is_forward = true
  end

module Analyzer = Stan.Analysis(Sign)

let show_node node env =
  Printf.printf "S%d -> { " node;
  pretty_env stdout env;
  Printf.printf "}\n%!"

let graph_file = ref "cfg.dot"
let input_file = ref ""

let usage = "Usage: live_var [-o cfg.dot] input_file"

let () =
  Arg.parse
    [ "-o", Arg.Set_string graph_file, "dot file output (default cfg.dot)" ]
    (fun s -> input_file := s)
    usage

let prog =
  if !input_file = "" then begin
    print_endline usage;
    exit 1
  end;
  let inchan = open_in !input_file in
  let res = Parser.program Lexer.main (Lexing.from_channel inchan) in
  close_in inchan;
  res

let (vars, ptrs) = Stan.vars_of_program prog

let env =
  Stan.StringSet.fold (fun v acc -> Env.add v Top acc) vars Env.empty
let env =
  Stan.StringSet.fold (fun v acc -> Env.add v Top acc) ptrs env

let graph, res = Analyzer.analyze prog env

let () =
  let outchan = open_out !graph_file in
  Stan.output_graph outchan graph;
  Analyzer.M.iter show_node res
